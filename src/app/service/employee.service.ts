import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee } from '../component/employee/employee';
import { EmployeeDetail } from '../component/employee/employeeDetail';
import { TimekeepingDTO } from '../component/checkin/timekeepingdto';
import { DayoffDTO } from '../component/add-day-off/dayOffDTO';
import { UpdateDayoffDTO } from '../component/add-day-off/updateDayOffDTO';
@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  constructor(private _http: HttpClient) {}
  getEmployee(): Observable<any> {
    return this._http.get('http://localhost:8080/employee');
  }
  getEmployeeByID(id: String): Observable<any> {
    return this._http.get('http://localhost:8080/Detail/' + id);
  }
  addCheckin(timekeepingDTO: TimekeepingDTO): Observable<any> {
    console.log(timekeepingDTO);
    return this._http.post('http://localhost:8080/add', timekeepingDTO);
  }
  addCheckOut(timekeepingDTO: TimekeepingDTO): Observable<any> {
    return this._http.put('http://localhost:8080/add_checkout', timekeepingDTO);
  }
  getDayoff(): Observable<any> {
    return this._http.get('http://localhost:8080/day_off');
  }

  addDayoff(_dayoffDTO: DayoffDTO): Observable<any> {
    return this._http.post('http://localhost:8080/day_off', _dayoffDTO);
  }
  updateDayOff(_dayoffDTO: UpdateDayoffDTO): Observable<any> {
    return this._http.put('http://localhost:8080/day_off', _dayoffDTO);
  }
}
