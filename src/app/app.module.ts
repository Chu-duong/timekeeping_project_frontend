import { NgModule } from '@angular/core';
import {
  BrowserModule,
  provideProtractorTestingSupport,
} from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { EmployeeComponent } from './component/employee/employee.component';
import { DetailComponent } from './component/detail/detail.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { CheckinComponent } from './component/checkin/checkin.component';
import { RouterModule, Routes, provideRouter } from '@angular/router';
import { AppRoutingModule } from './app-routing.modules';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { HttpClientModule } from '@angular/common/http';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import {
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatDialog,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatRadioModule } from '@angular/material/radio';
import { DialogModule, DialogRef } from '@angular/cdk/dialog';
import { DatePipe } from '@angular/common';
import { CheckoutComponent } from './component/checkout/checkout.component';
import { MatOption, MatOptionModule } from '@angular/material/core';
import { DayOffComponent } from './component/day-off/day-off.component';
import { AddDayOffComponent } from './component/add-day-off/add-day-off.component';
const appRoute: Routes = [
  { path: 'Employee', component: EmployeeComponent },
  { path: 'Checkin', component: CheckinComponent },
  { path: 'Detail', component: DetailComponent },
  { path: 'Day_off', component: DayOffComponent },
  {
    path: 'Detail/:id',
    component: DetailComponent,
    title: 'Home details',
  },
];
@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent,
    DetailComponent,
    CheckinComponent,
    CheckoutComponent,
    DayOffComponent,
    AddDayOffComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    HttpClientModule,
    MatButtonModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    DialogModule,
    MatDialogModule,
    DatePipe,
    MatSelectModule,
    MatOptionModule,
    MatDialogModule,
    RouterModule.forRoot(appRoute),
  ],
  providers: [
    DatePipe,
    { provide: MatDialogRef, useValue: { hasBackdrop: false } },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
