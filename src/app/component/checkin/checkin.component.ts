import { DialogRef } from '@angular/cdk/dialog';
import { Component, Inject, Input, OnInit, inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { TimekeepingDTO } from './timekeepingdto';
import { EmployeeService } from 'src/app/service/employee.service';
import { HttpErrorResponse } from '@angular/common/http';
@Component({
  selector: 'app-checkin',
  templateUrl: './checkin.component.html',
  styleUrls: ['./checkin.component.scss'],
})
export class CheckinComponent implements OnInit {
  empForm: FormGroup;
  @Input() item = ''; // decorate the property with @Input()
  constructor(
    private _fb: FormBuilder,
    private _dialogRef: MatDialogRef<CheckinComponent>,
    private _employeeService: EmployeeService,
    public datepipe: DatePipe,
    @Inject(MAT_DIALOG_DATA) public data: { id: string }
  ) {
    let currentDateTime = this.datepipe.transform(
      new Date(),
      'yyyy-MM-ddTHH:mm:ssZ'
    );
    this.empForm = this._fb.group({
      check_time: currentDateTime,
      employee_id: this.data.id,
    });
  }
  ngOnInit(): void {}

  onCheckIn() {
    if (this.empForm.valid) {
      // map form value to a specific dto before call api
      let Checkin: TimekeepingDTO = {
        checkIn: this.empForm.value.check_time,
        employee: this.data.id,
      };
      this._employeeService.addCheckin(Checkin).subscribe({
        next: (val: any) => {
          alert('Check-in successfully');
          this._dialogRef.close(true);
        },
        error: (error: HttpErrorResponse) => {
          alert(error.error.message);
        },
      });
    }
  }
}
