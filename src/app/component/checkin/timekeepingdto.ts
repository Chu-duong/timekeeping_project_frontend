export interface TimekeepingDTO {
  checkIn?: string;
  employee?: String;
  checkOut?: string;
  id?: String;
  date?: String;
}
