export interface EmployeeDetail {
  id: String;
  name: string;
  position: number;
  timekeepingset: [];
}
