export interface Employee {
  id: String;
  name: string;
  position: number;
}
