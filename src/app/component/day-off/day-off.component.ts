import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { EmployeeService } from 'src/app/service/employee.service';
import { AddDayOffComponent } from '../add-day-off/add-day-off.component';
import { UpdateDayoffDTO } from '../add-day-off/updateDayOffDTO';

@Component({
  selector: 'app-day-off',
  templateUrl: './day-off.component.html',
  styleUrls: ['./day-off.component.scss'],
})
export class DayOffComponent implements OnInit {
  dataSource: MatTableDataSource<any>;
  constructor(
    private _emplyService: EmployeeService,
    private _dialog: MatDialog
  ) {
    this.dataSource = new MatTableDataSource();
  }
  ngOnInit(): void {
    this.getDayOff();
  }
  displayedColumns: string[] = [
    'id',
    'employeeName',
    'employeeId',
    'date',
    'applicationName',
    'content',
    'state',
    'action',
  ];
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  serverData?: String[];
  getDayOff() {
    this._emplyService.getDayoff().subscribe({
      next: (res) => {
        const extractedAttribute: string[] = res.map(
          (item: { applicationName: any }) => item.applicationName
        );
        this.serverData = extractedAttribute;
        console.log(this.serverData);
        this.dataSource = new MatTableDataSource(res);
      },
      error: (err) => {
        console.log(err);
      },
    });
  }

  addDayOff() {
    let _dialogRefOut = this._dialog.open(AddDayOffComponent, {
      data: { application: this.serverData },
    });
    _dialogRefOut.afterClosed().subscribe({
      next: (val) => {
        if (val) {
          this.getDayOff();
        }
      },
    });
  }
  onApproved(id: number) {
    let update: UpdateDayoffDTO = {
      id: id,
      state: true,
    };
    this._emplyService.updateDayOff(update).subscribe({
      next: (res) => {
        alert('update successfully');
        this.getDayOff();
      },
      error: (err: any) => {
        console.error(err);
      },
    });
  }
}
