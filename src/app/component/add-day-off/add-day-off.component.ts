import { DatePipe } from '@angular/common';
import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from '@angular/material/dialog';
import { EmployeeService } from 'src/app/service/employee.service';
import { CheckinComponent } from '../checkin/checkin.component';
import { DayoffDTO } from './dayOffDTO';

@Component({
  selector: 'app-add-day-off',
  templateUrl: './add-day-off.component.html',
  styleUrls: ['./add-day-off.component.scss'],
})
export class AddDayOffComponent {
  constructor(
    private _fb: FormBuilder,
    private _dialogRef: MatDialogRef<CheckinComponent>,
    private _employeeService: EmployeeService,
    public datepipe: DatePipe,
    @Inject(MAT_DIALOG_DATA) public data: { application: string[] }
  ) {
    let currentDateTime = this.datepipe.transform(new Date(), 'yyyy-MM-dd');
    this.empForm = this._fb.group({
      check_time: currentDateTime,
      applications: '',
      content: '',
      employee_id: '',
      documentId: 0,
    });
  }
  applications: String[] = ['xin di muon', 'xin ve som', 'xin nghi'];
  document_id: number = 0;
  empForm: FormGroup;
  getId(): number {
    switch (this.empForm.value.applications) {
      case 'xin di muon':
        this.document_id = 1;
        break;
      case 'xin ve som':
        this.document_id = 2;
        break;
      case 'xin nghi':
        this.document_id = 3;
        break;
    }
    return this.document_id;
  }
  onAdd() {
    if (this.empForm.valid) {
      let dayOff: DayoffDTO = {
        date: this.empForm.value.check_time,
        state: false,
        content: this.empForm.value.content,
        employeeId: this.empForm.value.employee_id,
        document_id: this.getId(),
      };
      this._employeeService.addDayoff(dayOff).subscribe({
        next: (res) => {
          console.log(res);
          alert('Add day-off successfully');
          this._dialogRef.close(true);
        },
        error: (err: any) => {
          alert(err.error.message);
        },
      });
    }
  }
}
