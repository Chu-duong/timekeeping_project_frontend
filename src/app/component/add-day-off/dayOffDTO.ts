export interface DayoffDTO {
  date: string;
  state: boolean;
  content?: string;
  document_id: number;
  employeeId: String;
}
