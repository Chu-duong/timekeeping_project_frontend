export interface UpdateDayoffDTO {
  id: number;
  state: boolean;
}
