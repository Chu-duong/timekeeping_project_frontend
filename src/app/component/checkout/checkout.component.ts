import { DatePipe } from '@angular/common';
import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { EmployeeService } from 'src/app/service/employee.service';
import { Inject, OnInit, inject } from '@angular/core';
import { EmployeeDetail } from '../employee/employeeDetail';
import { TimekeepingDTO } from '../checkin/timekeepingdto';
@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
})
export class CheckoutComponent {
  empForm: FormGroup;
  @Input() item = ''; // decorate the property with @Input()
  constructor(
    private _fb: FormBuilder,
    private _dialogRef: MatDialogRef<CheckoutComponent>,
    private _employeeService: EmployeeService,
    public datepipe: DatePipe,
    @Inject(MAT_DIALOG_DATA) public data: { id: string }
  ) {
    let currentDateTime = this.datepipe.transform(
      new Date(),
      'yyyy-MM-ddTHH:mm:ssZ'
    );
    this.empForm = this._fb.group({
      CheckOut: currentDateTime,
      ID: '',
    });
  }
  ngOnInit(): void {}

  onCheckOut() {
    if (this.empForm.valid) {
      let CheckOut: TimekeepingDTO = {
        checkOut: this.empForm.value.CheckOut,
        id: this.data.id,
        date: this.empForm.value.CheckOut,
      };
      this._employeeService.addCheckOut(CheckOut).subscribe({
        next: (res) => {
          alert('Check-Out successfully');
          this._dialogRef.close(true);
        },
        error: (err: any) => {
          alert(err.error.message);
        },
      });
    }
  }
}
