import { Component, OnInit, inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmployeeService } from 'src/app/service/employee.service';
import { EmployeeComponent } from '../employee/employee.component';
import { Employee } from '../employee/employee';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CheckinComponent } from '../checkin/checkin.component';
import { DialogRef } from '@angular/cdk/dialog';
import { CheckoutComponent } from '../checkout/checkout.component';
import { EmployeeDetail } from '../employee/employeeDetail';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {
  route: ActivatedRoute = inject(ActivatedRoute);
  EmployeeID: string = String(this.route.snapshot.params['id']);
  numberLate: number = 0;
  numberSoon: number = 0;
  numberOut: number = 0;
  displayedColumns: string[] = ['date', 'time', 'checktype'];
  ngOnInit(): void {
    this.getDetails();
  }
  constructor(
    private _employeeService: EmployeeService,
    private _dialog: MatDialog
  ) {
    this.dataSource = new MatTableDataSource();
  }
  dataSource: MatTableDataSource<any>;
  serverData?: String[];
  getDetails() {
    this.numberLate = 0;
    this.numberSoon = 0;
    this.numberOut = 0;
    this._employeeService.getEmployeeByID(this.EmployeeID).subscribe({
      next: (res) => {
        this.dataSource = new MatTableDataSource(res);

        const extractedAttribute: string[] = res.map(
          (item: { id: any }) => item.id
        );
        const extractedAttribute_state: string[] = res.map(
          (item: { state: any }) => item.state
        );
        extractedAttribute_state.forEach((e) => {
          if (e === 'di muon' || e === 'di muon ve som') {
            this.numberLate++;
          }
          if (e === 've som' || e === 'di muon ve som') {
            this.numberSoon++;
          }
          if (e === 'nghi khong phep') {
            this.numberOut++;
          }
        });

        this.serverData = extractedAttribute;
      },
      error: (err) => {
        console.log(err);
      },
    });
  }

  openCheckin() {
    let _dialogRef = this._dialog.open(CheckinComponent, {
      data: { id: this.EmployeeID },
    });
    _dialogRef.afterClosed().subscribe({
      next: (val) => {
        if (val) {
          this.getDetails();
        }
      },
    });
  }
  openCheckOut() {
    let _dialogRefOut = this._dialog.open(CheckoutComponent, {
      data: { id: this.EmployeeID },
    });
    _dialogRefOut.afterClosed().subscribe({
      next: (val) => {
        if (val) {
          this.getDetails();
        }
      },
      error: (err) => {
        console.log(err.error.message);
      },
    });
  }
}
