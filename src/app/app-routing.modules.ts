import { NgModule } from '@angular/core';
import { NgModel } from '@angular/forms';
import { Route, RouterModule, Routes } from '@angular/router';
import { EmployeeComponent } from './component/employee/employee.component';
import { DetailComponent } from './component/detail/detail.component';

const routes: Routes = [
  { path: 'employee', component: EmployeeComponent },
  { path: 'details', component: DetailComponent },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
